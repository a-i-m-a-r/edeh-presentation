/**
 * A plugin which enables rendering of a count down clock in
 * reveal.js slides.
 *
 * @author Christer Eriksson
 */
var RevealCountDown =
  window.RevealCountDown ||
  (function() {
    var options = Reveal.getConfig().countdown || {};

    var defaultOptions = {
      defaultTime: 300,
      autostart: "no",
    };

    defaults(options, defaultOptions);

    function defaults(options, defaultOptions) {
      for (var i in defaultOptions) {
        if (!options.hasOwnProperty(i)) {
          options[i] = defaultOptions[i];
        }
      }
    }

    var counterRef = null;
    var interval = null;
    var startTime = 0;
    var elapsedTime = 0;
    var running = false;

    Reveal.addEventListener("slidechanged", function(event) {
      initCountDown(event.currentSlide);
    });

    // If timer is on first slide start on ready
    Reveal.addEventListener("ready", function(event) {
      initCountDown(event.currentSlide);
    });


    function updateTimer(timeLeft) {
      if (counterRef === null) return;
      secondsLeft = timeLeft;
      minutesLeft = Math.floor(secondsLeft / 60);
      secondsLeft = secondsLeft % 60;
      hoursLeft = Math.floor(minutesLeft / 60);
      minutesLeft = minutesLeft % 60;

      if (hoursLeft > 0) {
        counterRef.innerHTML =
          hoursLeft + " h " + minutesLeft + " m " + secondsLeft + " s";
        return;
      }
      if (minutesLeft > 0) {
        counterRef.innerHTML = minutesLeft + " m " + secondsLeft + " s";
        return;
      }
      if (minutesLeft <= 0) {
        counterRef.innerHTML = secondsLeft + " s";
        return;
      }
    }

    function startTimer() {
      interval = setInterval(async function() {
        if (elapsedTime < startTime && running && !Reveal.isPaused()) {
          elapsedTime = elapsedTime + 1;
          updateTimer(startTime - elapsedTime);
        }
      }, 1000);
    }

    function initCountDown(currentSlide) {
      if (interval != null) clearInterval(interval);
      counterRef = currentSlide.getElementsByTagName("countdown")[0];
      if (counterRef === undefined) return;
      time = counterRef.getAttribute("time");
      autostart = counterRef.getAttribute("autostart");
      elapsedTime = 0;
      startTime = time ? time : options.defaultTime;
      startTimer();
      updateTimer(startTime - elapsedTime);
      running = autostart === "yes" ? true : false;
    }

    return {
      init: function() {}
    };
  })();

Reveal.registerPlugin("countdown", RevealCountDown);
